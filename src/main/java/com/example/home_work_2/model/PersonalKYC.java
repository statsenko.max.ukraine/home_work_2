package com.example.home_work_2.model;

import com.example.home_work_2.ReaderCSV;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class PersonalKYC {
    private final Person person;
    private final Map<String, String> questionsAndAnswers = new HashMap<>();

    public PersonalKYC(Person person) {
        this.person = person;

        List<String> questions = ReaderCSV.readCSVAllDataAtOnce("src/main/resources/questions.csv");

        questions.forEach(question -> {
            System.out.println(question);
            Scanner scanner = new Scanner(System.in);
            System.out.println("Your answer: ");
            questionsAndAnswers.put(question, scanner.nextLine());
        });
    }

    @Override
    public String toString() {
        return person +
                "Questions and answers = " + questionsAndAnswers;
    }
}
