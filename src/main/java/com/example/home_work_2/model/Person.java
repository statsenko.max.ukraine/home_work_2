package com.example.home_work_2.model;

import java.util.Scanner;

public class Person {
    private final String firstName;
    private final String lastName;

    public Person() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter firstName");
        this.firstName = scanner.nextLine();
        System.out.println("Enter lastName");
        this.lastName = scanner.nextLine();
    }

    public String toString() {
        return "FirstName = " + this.firstName + ", LastName = " + this.lastName + "\n";
    }
}
